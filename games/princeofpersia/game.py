#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os
from subprocess import check_output

from tools import sandbox

recultis_dir = os.getenv("HOME") + "/.recultis/"
self_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
install_dir = recultis_dir + "SDLPoP/"
desk_dir = str(check_output(['xdg-user-dir', 'DESKTOP']))[2:-3] + "/"

full_name = "Prince of Persia on SDLPoP"
description = """Prince of Persia is an action-adventure game, in which
you must explore dungeons full of traps, defeat enemies
in sword fight and solve puzzles. The dungeon is full of potions
and other items you can interact with. The game consists of twelve
levels. This game port is based on original code, which was
adapted by SDLPoP project. It doesn't require
original game files.
"""

shops = ["none"]
screenshot_path = self_dir + "../../assets/html/sdlpop-screen.png"
icon1_name="sdlpop.png"
icon_list = [icon1_name]

engine = "sdlpop"
runtime_version = 2
env_var = "LD_LIBRARY_PATH=$HOME/.recultis/runtime/recultis" + str(runtime_version) + ":$HOME/.recultis/runtime/recultis" + str(runtime_version) + "/custom"
launcher1_cmd = "bash -c 'cd $HOME/.recultis/SDLPoP/; " + env_var + " ./prince'"
launcher_cmd_list = [["Price of Persia", launcher1_cmd]]
launcher1_text = """[Desktop Entry]
Type=Application
Name=Prince of Persia on SDLPoP
Comment=Play Prince of Persia on SDLPoP
Exec=""" + launcher1_cmd + """
Icon=""" + icon1_name + """
Categories=Game;
Terminal=false
"""

launcher_list = [["sdlpop.desktop", launcher1_text]]

uninstall_files_list = []
uninstall_dir_list = []

def prepare_engine():
	print("Preparing game engine")
	#Copy game engine
	if sandbox.ispresent(install_dir):
		sandbox.rm(install_dir)
	sandbox.cp(recultis_dir + "tmp/sdlpop/", install_dir)
	print("Game engine ready")
