#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os
from subprocess import check_output

from tools import sandbox

recultis_dir = os.getenv("HOME") + "/.recultis/"
self_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
install_dir = recultis_dir + "warzone2100/"
desk_dir = str(check_output(['xdg-user-dir', 'DESKTOP']))[2:-3] + "/"

full_name = "Warzone 2100"
description = """In Warzone 2100, you command the forces of The Project in a
battle to rebuild the world after mankind has almost been
destroyed by nuclear missiles.
The game offers campaign, multi-player, and single-player
skirmish modes. An extensive tech tree with over 400 different
technologies, combined with the unit design system, allows for
a wide variety of possible units and tactics.
*from wz2100.net"""

shops = ["none"]
screenshot_path = self_dir + "../../assets/html/warzone2100-screen.png"
icon1_name="warzone2100.png"
icon_list = [icon1_name]

engine = "warzone2100"
runtime_version = 2
env_var = "LD_LIBRARY_PATH=$HOME/.recultis/runtime/recultis" + str(runtime_version) + ":$HOME/.recultis/runtime/recultis" + str(runtime_version) + "/custom:$HOME/.recultis/runtime/recultis" + str(runtime_version) + "/qt5 QT_PLUGIN_PATH=$HOME/.recultis/runtime/recultis" + str(runtime_version) + "/qt5/plugins"
launcher1_cmd = "bash -c 'cd $HOME/.recultis/warzone2100/bin/; " + env_var + " ./warzone2100'"
launcher_cmd_list = [["Warzone2100", launcher1_cmd]]
launcher1_text = """[Desktop Entry]
Type=Application
Name=Warzone2100
Comment=Play Warzone2100
Exec=""" + launcher1_cmd + """
Icon=""" + icon1_name + """
Categories=Game;
Terminal=false
"""

launcher_list = [["warzone2100.desktop", launcher1_text]]

uninstall_files_list = []
uninstall_dir_list = []

def prepare_engine():
	print("Preparing game engine")
	#Copy game engine
	if sandbox.ispresent(install_dir):
		sandbox.rm(install_dir)
	sandbox.cp(recultis_dir + "tmp/warzone2100/", install_dir)
	#TODO: Add cinematic download
	print("Game engine ready")
