#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os
from subprocess import check_output

from tools import sandbox

recultis_dir = os.getenv("HOME") + "/.recultis/"
self_dir = os.path.dirname(os.path.abspath(__file__)) + "/"
install_dir = recultis_dir + "Quake3/"
desk_dir = str(check_output(['xdg-user-dir', 'DESKTOP']))[2:-3] + "/"

full_name = "Quake 3 on ioquake3 engine"
description = """Quake III Arena is one of the greatest multiplayer
fast first person shooters. Well balanced, build on established
Quake franchise is one of the most successful games
of its type. This FPS not only provides great
multiplayer but also fun and challenging fights on singleplayer
maps. This game is build on ioquake3 game engine, which
provides multiple advantages over original game.
"""

shops = ["steam", "gog"]
s_appid = "2200"
steam_link =  "http://store.steampowered.com/app/"+s_appid+"/"
g_appid = "quake_iii_arena_and_team_arena"
gog_link = "https://www.gog.com/game/quake_iii_gold"
screenshot_path = self_dir + "../../assets/html/ioquake3-screen.png"
icon1_name = "ioquake3.png"
icon_list = [icon1_name]

engine = "ioquake3"
runtime_version = 2
env_var = "LD_LIBRARY_PATH=$HOME/.recultis/runtime/recultis" + str(runtime_version) + ":$HOME/.recultis/runtime/recultis" + str(runtime_version) + "/custom"
launcher1_cmd = "bash -c 'cd $HOME/.recultis/Quake3; " + env_var + " ./ioquake3.x86_64'"
launcher_cmd_list = [["Quake3", launcher1_cmd]]
launcher1_text = """[Desktop Entry]
Type=Application
Name=Quake 3
Comment=Play Quake 3
Exec=""" + launcher1_cmd + """
Icon=""" + icon1_name + """
Categories=Game;
Terminal=false"""
launcher_list = [["quake3.desktop", launcher1_text]]

uninstall_files_list = []
uninstall_dir_list = []

def prepare_engine():
	#Here is all game specific code
	print("Preparing game engine")
	#if GOG
	if sandbox.ispresent(install_dir + "app")[0] == True:
		for quake3_file_or_dir in os.listdir(install_dir + "app"):
			if sandbox.ispresent(install_dir + quake3_file_or_dir)[0] == True:
				sandbox.rm(install_dir + quake3_file_or_dir)
			sandbox.mv(install_dir + "app/" + quake3_file_or_dir, install_dir + quake3_file_or_dir)
		sandbox.rm(install_dir + "app")
	#Actual install
	for ioq3_file_or_dir in os.listdir(recultis_dir + "tmp/ioquake3"):
		if sandbox.ispresent(recultis_dir + "tmp/ioquake3/" + ioq3_file_or_dir)[1] == "file":
			if sandbox.ispresent(install_dir + ioq3_file_or_dir)[0] == True:
				sandbox.rm(install_dir + ioq3_file_or_dir)
			sandbox.cp(recultis_dir + "tmp/ioquake3/" + ioq3_file_or_dir, install_dir + ioq3_file_or_dir)
		elif sandbox.ispresent(recultis_dir + "tmp/ioquake3/" + ioq3_file_or_dir)[1] == "dir":
			ioq3_dir_content = os.listdir(recultis_dir + "tmp/ioquake3/" + ioq3_file_or_dir)
			for ioq3_file_in_dir in ioq3_dir_content:
				if sandbox.ispresent(install_dir + ioq3_file_or_dir + "/" + ioq3_file_in_dir)[0] == True:
					sandbox.rm(install_dir + ioq3_file_or_dir + "/" + ioq3_file_in_dir)
				sandbox.cp(recultis_dir + "tmp/ioquake3/" + ioq3_file_or_dir + "/" + ioq3_file_in_dir, install_dir + ioq3_file_or_dir + "/" + ioq3_file_in_dir)
	print("Game engine ready")
