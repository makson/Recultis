# Recultis README

## Recultis: Effortlessly Integrate Proprietary Content into Open-Source Games

**What is Recultis?**  
Recultis empowers you to seamlessly blend your own creations or proprietary assets with the rich worlds of open-source game engines. Through its intuitive GUI, Recultis streamlines the process of adding custom content to various popular games, enriching your gameplay experience.

Visit our [Project Website](https://makson.gitlab.io/Recultis/)

### Supported Games:
- Aliens vs Predator Classic 2000 (AVP engine)
- Command & Conquer Series (OpenRA engine)
- Doom 3 BFG Edition (RBDoom-3-BFG engine)
- Heroes of Might and Magic III Complete (VCMI engine)
- Jedi Knight: Jedi Academy & Jedi Outcast (OpenJK engine)
- The Elder Scrolls III: Morrowind (OpenMW engine)
- Prince of Persia (SDLPoP engine)
- Quake & Quake II (Darkplaces & Yamagi Quake II engines)
- Quake III Arena (ioquake3 engine)
- Warzone 2100
- X-COM: Terror From the Deep & UFO Defense (OpenXcom engine)
- If your favorite game is still missing, please let us know!

### Key Features:
- **Intuitive GUI:** Effortlessly manage and integrate your proprietary content without complex scripting or coding.
- **Wide Game Engine Support:** Expand your creativity across a diverse range of popular open-source titles.
- **Streamlined Workflow:** Simplify the process of adding custom assets, textures, models, and more.
- **Community-Driven:** Benefit from and contribute to an active community of modders and enthusiasts.

### Getting Started:
This application needs Python3 and PyQt5 to display GUI. The games require a 64-bit system. They are portable and will work on any Linux distribution. Steamcmd requires 32 bit libc, libstdc++, libgcc_s.

#### Prerequisites:
- [Python 3](https://www.python.org/)
- [PyQt5](https://riverbankcomputing.com/software/pyqt/)
- SteamCMD (may be required for certain games)

#### Installation:
Detailed instructions are available on the [project's website](https://makson.gitlab.io/Recultis/). Consider offering pre-built binaries or Docker images for easier setup.

#### Usage Guide:
Refer to the comprehensive user guide on the website for step-by-step instructions on adding content to different game engines. Include code snippets or screenshots where relevant.

#### Troubleshooting:
Consult the FAQ on the website for common issues and solutions. Provide clear instructions for reporting bugs and seeking help.

### Community and Support:
Join the Recultis community forum to connect with other users, share your creations, and get help.  
Report bugs and suggest improvements on the project's [GitLab repository](https://gitlab.com/makson/Recultis).

### License
This software is available to you under the terms of the GPL-3. See "/usr/share/common-licenses/GPL-3".

**Copyright:**  
Tomasz Makarewicz (tomasz.makarewicz@protonmail.ch)
