#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

import os, urllib.request, tarfile, importlib, socket

from tools import sandbox

#Status: -1 - Checking for update...; 0 - Not installed; 1 - Installed; 2 - Update available; 3 - Installing...	
def game_update_desc(info_list):
	game_fname = info_list[0]
	status = info_list[1]
	game_module = importlib.import_module("games." + game_fname + ".game")
	full_name = game_module.full_name
	rest_name = ""
	if status == -1:
		rest_name = " (Checking for update...)"
	elif status == 0:
		rest_name = " (Not installed)"
	elif status == 1:
		rest_name = " (Installed)"
	elif status == 2:
		rest_name = " (Update available)"
	elif status == 3:
		rest_name = " (Installing...)"
	return full_name + rest_name

#This function will return game status and update link if possible.
def game_update_status(game_list, recultis_dir):
	link_string_list = get_link_list(game_list)
	from games import installer
	status_list = []
	game_nr = 0
	for game in game_list:
		game_info = installer.game_info(game, ["version"])
		version = game_info[0]
		if version != "No proper install":
			status = 1
			if version != link_string_list[game_nr]:
				status = 2
		else:
			status = 0
		game_nr += 1
		print(game + " status is " + str(status))
		status_list.append(status)
	return status_list

def get_link_list(package_list):
	print("Getting projects download link")
	target_page = urllib.request.urlopen("https://launchpad.net/~makson96/+archive/ubuntu/recultis/+builds?build_state=built&build_text=&batch=200")
	target_page_str = str(target_page.read())
	download_link_list = []
	for package in package_list:
		try:
			game_module = importlib.import_module("games." + package + ".game")
			runtime_version = game_module.runtime_version
			target_package = game_module.engine
		except ModuleNotFoundError:
			target_package = package
			runtime_version = 2
		if runtime_version == 2:	
			start = target_package + " "
			end = " in ubuntu xenial"
			target_end_list= target_page_str.split(end)
			for xenial_build in target_end_list:
				if start in xenial_build:
					target_start_list = xenial_build.split(start)[-1]
					break
			target_version = target_start_list
			download_link = "https://launchpad.net/~makson96/+archive/ubuntu/recultis/+files/" + target_package + "_" + target_version + "_amd64.deb"
			download_link_list.append(download_link)
	print("Downalod links are:")
	print(download_link_list)
	return download_link_list

def recultis_update_do(self_dir, patch_link):
	print("Starting autoupdate.")
	#download patch
	patch_file = "patch.tar.gz"
	urllib.request.urlretrieve(patch_link, self_dir + patch_file)
	#remove old files
	for the_file in os.listdir(self_dir):
		file_path = os.path.join(self_dir, the_file)
		if sandbox.ispresent(file_path)[0] and the_file != patch_file:
			sandox.rm(file_path, [self_dir])
	#unpack patch
	tar = tarfile.open(self_dir + patch_file)
	tar.extractall()
	tar.close()
	#remove patch file
	sandbox.rm(self_dir + patch_file, [self_dir])
	#copy everything one directory backs
	patch_dir = os.listdir(self_dir)[0]
	for the_file_or_dir in os.listdir(patch_dir):
		sandbox.mv(os.path.join(patch_dir, the_file_or_dir), os.path.join(self_dir, the_file_or_dir), [self_dir])
	sandbox.rm(patch_dir, self_dir)
	print("Autoupdate complete.")

def recultis_update_check(self_dir, recultis_version):
	print("Checking if Recultis update is available")
	v_major = str(int(recultis_version[0]) + 1) + ".0.0"
	v_minor = recultis_version[0:2] + str(int(recultis_version[2]) + 1) + ".0"
	v_patch = recultis_version[0:4] + str(int(recultis_version[4]) + 1)
	update_list = [v_major, v_minor, v_patch]
	if sandbox.ispresent(self_dir + "patch_link.txt")[0]:
		sandbox.rm(self_dir + "patch_link.txt", [self_dir])
	status = 1
	target_v_page = urllib.request.urlopen("https://gitlab.com/makson/Recultis/tags")
	target_v_page_str = str(target_v_page.read())
	for potential_patch in update_list:
		v_present = True
		v0_present = False
		while v_present:
			if "Recultis-v" + potential_patch + ".tar.gz" in target_v_page_str:
				v_present = True
				v0_present = True
				status = 2
				patch_plus_1 = potential_patch.split(".")
				patch_plus_1[2] = str(int(patch_plus_1[2]) + 1)
				potential_patch = ".".join(patch_plus_1)
			else:
				v_present = False
				if v0_present == True:
					patch_minus_1 = potential_patch.split(".")
					patch_minus_1[2] = str(int(patch_minus_1[2]) - 1)
					potential_patch = ".".join(patch_minus_1)
					patch_url = "https://gitlab.com/makson/Recultis/-/archive/v" + potential_patch + "/Recultis-v" + potential_patch + ".tar.gz"
					patch_link_file = open(self_dir + "patch_link.txt", "a")
					patch_link_file.write(patch_url + "\n")
					patch_link_file.close()
					print("Found following Recultis patch:")
					print(patch_url)
	print("Return Recultis update status: " + str(status))
	return status
