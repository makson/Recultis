#!/usr/bin/env python3
#-*- coding: utf-8 -*-

##This software is available to you under the terms of the GPL-3, see "/usr/share/common-licenses/GPL-3".
##Copyright:
##- Tomasz Makarewicz (makson96@gmail.com)

##This file will move icon to proper directory, check dependencies and handle autoupdate.

import os, sys

from tools import sandbox

#Copy icon
if sandbox.ispresent(os.getenv("HOME") + "/.local/share/icons")[0] == False:
	sandbox.mkdir(os.getenv("HOME") + "/.local/share/icons", [os.getenv("HOME") + "/.local/share/icons"])
if sandbox.ispresent(os.getenv("HOME") + "/.local/share/icons/recultis.png")[0] == False:
	print("Prepare Recultis launcher icon.")
	sandbox.cp(os.path.dirname(os.path.abspath(__file__)) + "/assets/icon.png", os.getenv("HOME") + "/.local/share/icons/recultis.png", [os.getenv("HOME") + "/.local/share/icons/"])

#Create ~./recultis directory
recultis_dir = os.getenv("HOME") + "/.recultis/"
if sandbox.ispresent(recultis_dir)[0] == False:
	sandbox.mkdir(recultis_dir)

#Check for dependencies
print("Checking dependencies")
mandatory_dependencie=True
optional_dependencie=True
#Mandatory
#64bit system
import struct
dep_64bit = struct.calcsize('P') * 8 == 64
del struct
#Python3 PyQt5
try:
	import PyQt5
	del PyQt5
	dep_pyqt = True
	print("PyQt5 found")
except:
	dep_pyqt = False
	print("PyQt5 not found")
#DPKG or AR
from tools import unpack_deb
dep_dpkg = unpack_deb.check_dpkg()
if dep_dpkg == True:
	print("dpkg found")
else:
	print("dpkg not found")
dep_ar = unpack_deb.check_ar()
if dep_ar == True:
	print("ar found")
else:
	print("ar not found")
del unpack_deb

#Optional
ldconfig_command = "ldconfig"
if sandbox.ispresent("/sbin/ldconfig")[0]:
	ldconfig_command = "/sbin/ldconfig"
#Steam
#32 bit libc and stdlic++
ld_32bit_libc = os.popen(ldconfig_command + " -p | grep libc.so").read()
dep_32bit_libc = len(ld_32bit_libc.split("\n\t")) == 2
if dep_32bit_libc == True:
	print("32 bit libc found")
else:
	print("32 bit libc not found")
ld_32bit_libstd = os.popen(ldconfig_command + " -p | grep libstdc++.so").read()
dep_32bit_libstd = len(ld_32bit_libstd.split("\n\t")) == 2
if dep_32bit_libstd == True:
	print("32 bit libstdc++ found")
else:
	print("32 bit libstdc++ not found")
ld_32bit_libgcc = os.popen(ldconfig_command + " -p | grep libgcc_s.so").read()
dep_32bit_libgcc = len(ld_32bit_libgcc.split("\n\t")) == 2
if dep_32bit_libgcc == True:
	print("32 bit libgcc_s found")
else:
	print("32 bit libgcc_s not found")	
#OpenAL
ld_openal = os.popen("ldconfig -p | grep openal.so").read()
dep_openal = ld_openal != ""
if dep_openal == True:
	print("OpenAL found")
else:
	print("OpenAL not found")	

#Remove old warning_file
if sandbox.ispresent(recultis_dir + "warning_file.txt")[0]:
	sandbox.rm(recultis_dir + "warning_file.txt")

#Prepare and display error messages
global dep_error
dep_error = ""
if dep_64bit == False:
	mandatory_dependencie=False
	dep_error = dep_error + "Error: You don't have 64 bit system, which is required for all games to run.\n"
if dep_pyqt == False:
	mandatory_dependencie=False
	dep_error = dep_error + "Error: Python3 PyQt5 is missing. Please install it from the repository and start the program again.\n"
if dep_dpkg == False and dep_ar == False:
	mandatory_dependencie=False
	dep_error = dep_error + "Error: both 'ar' and 'dpkg' are missing in the system. Please install one of them and start the program again.\n"
if dep_32bit_libc == False or dep_32bit_libstd == False or dep_32bit_libgcc == False:
	optional_dependencie=False
	dep_error = dep_error + "Warning: 32 bit libc required for Steam is missing. Try to install lib32gcc1 and lib32stdc++6 on Ubuntu/Debian or glibc.i686 and libstdc++.i686 on Fedora.\n"
if dep_openal == False:
	optional_dependencie=False
	dep_error = dep_error + "Warning: OpenAL required to have sound in games is missing. Try to install libopenal1 on Ubuntu/Debian or openal-soft on Fedora.\n"
if mandatory_dependencie == False:
	print(dep_error)
	terminal_ok = os.popen("x-terminal-emulator -e 'echo -e \""+dep_error+" \" && read -n 1 -s'")
	if termina_ok != "0":
		os.popen("xterm -e 'echo -e \""+dep_error+" \" && read -n 1 -s'")
	sys.exit(2)
elif optional_dependencie == False:
	#If we could do this without a file write, it would be great.
	warning_file = open(recultis_dir + "warning_file.txt", "w")
	warning_file.write(dep_error + "\n")
	del warning_file

#Start main program
print("Every dependencie met. Starting Recultis.")
import recultis.py
